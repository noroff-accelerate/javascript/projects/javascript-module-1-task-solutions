// Switch to the branch that contains the task solution code using
// git checkout <name-of-task>
// for example
// git checkout Calculate-the-Total-Price-of-a-Coffee-Order

// A list of the branch names can be found in the repo 
// OR run - git branch - to see all branches

// Calculate-the-Total-Price-of-Coffee-Multiple-Orders
// Calculate-the-Total-Price-of-a-Coffee-Order
// Manage-Coffee-Orders-from-an-API-with-Async-and-Await
// Manage-Coffee-Orders-from-an-API-with-Promises
// Manage-Coffee-Orders-with-Array-Operations
// Manage-Coffee-Orders-with-Built-in-JS-Higher-Order-Functions
// Manage-Coffee-Orders-with-Closures